import React, { Component } from 'react';
import service from './service';
import {Link} from 'react-router-dom';

class Car extends Component {
    constructor(props) {
        super(props);

    }
    componentWillMount() {
        this.state = { service: service }
        console.log(this.state);
        debugger;
    }
    render() {
        
           const listOfData=this.state.service.map((item,index) => {
              return <div className="card col-md-2  text-white" style={{ cursor:"pointer"}}>
                   <Link to={"/cars/"+item.id}>
                   <div class="card-body">{item.name}</div>
                   </Link> 
               </div>
            });
        


        return (
            <div class="container">
                <h2>Car Models</h2>
                {listOfData}
                
             </div>

                    );
               }
           }
            
export default Car;