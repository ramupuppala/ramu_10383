import React, { Component } from 'react';
import img from '../car.png';

export default [
    {
        id:1,        
        name:"Honda",
        description:"There are costs and benefits to car use. The costs include acquiring the vehicle, interest payments (if the car is financed), repairs and maintenance, fuel, depreciation, driving time, parking fees, taxes, and insurance.",
        fuelCapacity:20,
        speed:140,
        weightTotal:200,
        cost:1000000,
        img:img

    },
    {
        id:2,
        name:"Maruthi",
        description:"There are costs and benefits to car use. The costs include acquiring the vehicle, interest payments (if the car is financed), repairs and maintenance, fuel, depreciation, driving time, parking fees, taxes, and insurance.",
        fuelCapacity:18,
        speed:220,
        weightTotal:190,
        cost:770000,
        img:img

    },
    {
        id:3,
        name:"BMW",
        description:"There are costs and benefits to car use. The costs include acquiring the vehicle, interest payments (if the car is financed), repairs and maintenance, fuel, depreciation, driving time, parking fees, taxes, and insurance.",
        fuelCapacity:40,
        speed:400,
        weightTotal:400,
        img:img

    },
    {
        id:4,
        name:"Hundai",
        description:"There are costs and benefits to car use. The costs include acquiring the vehicle, interest payments (if the car is financed), repairs and maintenance, fuel, depreciation, driving time, parking fees, taxes, and insurance.",
        fuelCapacity:22,
        speed:170,
        weightTotal:300,
        cost:6000000,
        img:img

    },
    {
        id:5,
        name:"Nano",
        description:"There are costs and benefits to car use. The costs include acquiring the vehicle, interest payments (if the car is financed), repairs and maintenance, fuel, depreciation, driving time, parking fees, taxes, and insurance.",
        fuelCapacity:25,
        speed:150,
        weightTotal:180,
        cost:1000000,
        img:img

    }
  ];
  