import React, { Component } from 'react';
import service from './service';

class CarDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
        id:props.match.params.id,
        service: service
        }
 
    }
    componentWillMount() {
    
      
    }
    render() {
        
        const listOfData=this.state.service.map((item,index) => {
            if(this.state.id==item.id)
            {
                return <div className="card col-5" style={{marginLeft:"400px"}}>
                    <img class="card-img-top" src={item.img} alt="Card image" />
                    <div class="card-body">
                    <h4 class="card-title"><b>Name: </b>{item.name}</h4>
                        <p class="card-text"><b>Description: </b>{item.description}</p>
                        <p class="card-text"><b>FuelCapacity: </b>{item.fuelCapacity}</p>
                        <p class="card-text"><b>Speed: </b>item.speed</p>
                        <p class="card-text"><b>Weight Total: </b>{item.weightTotal}</p>
                        <p class="card-text"><b>Cost: </b>{item.cost}</p>
                        
                    </div>
                </div>
            }     


          });

        return (
            <div style={{marginTop:"50px"}}>
                
               {listOfData}
            </div>


        );
    }
}

export default CarDetails;