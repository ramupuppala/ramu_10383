import React, { Component } from 'react';

import { BrowserRouter ,Route,Switch} from 'react-router-dom';
import CarDetails from './components/cardetails';
import Car from './components/car';

const  Routes =()=>(

    <BrowserRouter>
    <Switch>
    <Route path="/cars" exact component={Car}/>
    <Route path="/cars/:id" exact component={CarDetails}/>
    </Switch>
    </BrowserRouter>
)

export default Routes;
