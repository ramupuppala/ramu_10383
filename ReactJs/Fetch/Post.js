import React, { Component } from 'react';

class Postform extends Component {
    constructor(props) {
        super(props);
        this.state = { 
           
               title:"",
               body:""
           
        }
    }

    
    handleInut(event) {
      
        const inputValue = event.target.value;
        const name = event.target.name;
        this.setState({
        
            [name]: inputValue
          
        })
    
      }
      onSubmit(e){
          e.preventDefault();
          const post={
              title:this.state.title,
              body:this.state.body
          }

          fetch('https://jsonplaceholder.typicode.com/posts',{
              method:'POST',
              headers:{
                  'content-type':'application/json'
              },
              body:JSON.stringify(post)
          }
          )
        .then(res=>res.json())
        .then(data=>console.log(data));
      }
    render() { 
        return ( 
            <form >
                <h3>Drop Us a Message</h3>
               <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                        <label for="title">Title</label>
                            <input type="text" name="title" className="form-control" onChange={(event) => this.handleInut(event)} placeholder="Title *" value="" />
                        </div>
                                       
                        </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                        <label for="body">Body</label>
                            <textarea name="body" class="form-control" placeholder="your body *" onChange={(event) => this.handleInut(event)} style={{width: "",height: "150px"}}></textarea>
                        </div>
                    </div>
                    
                </div>
                <div class="form-group">
                            <input type="submit" name="btnSubmit" className="btnContact btn btn-success" onClick={this.onSubmit.bind(this)} value="Send Message" />
            </div>
            </form>
            
         );
    }
}
 
export default Postform;
