import React, { Component } from 'react';
import {connect} from 'react-redux';

import { fetchPosts } from '../actions/postActions'

class Path extends Component {
    constructor(){
        this.state={
            posts:[]
        }
    }
    componentWillMount(){
        fetch('https://jsonplaceholder.typicode.com/posts')
        .then(res=>res.json())
        .then(data=>this.setState({posts:data} ));
    }
     
    render() { 
        const postItems=this.state.posts.map(item=>(
          <div key={item.id}  >
          <h3>{item.title}</h3>
          <p>{item.body}</p>
          </div>
        ));
        return (  
            <div>
                    <h1>Postitems</h1>
                     {postItems}
            </div>

        );
    }
}
 
export default connect(null,{fetchPosts})(Path);
