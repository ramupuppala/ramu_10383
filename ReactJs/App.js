import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import CRUD from './components/crud';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
        <CRUD />
        </header>
      </div>
    );
  }
}

export default App;
