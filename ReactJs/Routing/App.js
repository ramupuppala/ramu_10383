import React, { Component } from 'react';
import Crud from './components/crud';


import { BrowserRouter as Router, Route, Link ,NavLink,Redirect,Prompt} from 'react-router-dom';

const Home = () => <h1><Link to="/about">Click Me</Link></h1>
const About = () => <h1>About Us</h1>


const User=(params)=>{
  return ( <h1>Welcome user {params.username}</h1>)
}

class App extends Component{


    state={
      loggin:false 
    }
 

  logHandle=()=>{
    this.setState(preState => ({
      loggin:!preState.loggin
    }))
    console.log(this.state.loggin);
  }

render(){
  return(
    <Router>
      <div>
        <NavLink to="/student"> Student</NavLink>
        <NavLink to="/"> Home</NavLink>
        <NavLink to="/user/petter"> peter</NavLink>
        <NavLink to="/user/john"> john</NavLink>
        <Prompt when={!this.state.loggin} message={(location)=>{return location.pathname.startsWith('/user') ? 'are you sure?':true}}/>
        
        <input type="button" class="btn btn-primary" value={ this.state.loggin? "logout" :"login"} onClick={this.logHandle.bind(this)}></input>
      
      <Route exact path="/" component={Home}/>
       <Route path="/about" component={About} />
       <Route path="/student" component={Crud}/>
     
       <Route path="/user/:username" exact strict render={({match})=>(
         this.state.loggin? (<User username={match.params.username}/>) : (<Redirect to="/" />)
       )}/>
       
      </div>
       
    </Router>
  )
}
}
export default App;