import React, { Component } from 'react';
import { render } from 'react-dom';
import './style.css';

import DisplayCrud from './displaycrud';

class Crud extends Component {
    constructor() {
        super();
        this.state = {
          name: 'React',
          activeIndex:0,
          UserData: [],
          inputs: {
            name: "",
            age: "",
            DOB: ""
          }
        };
      } 


    
      handleInut(event) {
      
        const inputValue = event.target.value;
        const name = event.target.name;
        this.setState({
          inputs: {
            ...this.state.inputs,
            [name]: inputValue
          }
        })
    
      }
    
      addUser() {
        let data = this.state.UserData;
        data.push(this.state.inputs);
        
        this.setState({ UserData: data })

        console.log(this.state.UserData);
       
      }

      
      
    
    
      render() {
       
    
        return (
          <div>
           
            <table >
              <tr>
                <td>Name</td>
                <td><input name="name"onChange={(event) => this.handleInut(event)} value={this.state.inputs.name} /></td>
              </tr>
              <tr>
                <td>Age</td>
                <td><input name="age" onChange={(event) => this.handleInut(event)} value={this.state.inputs.age} /></td>
              </tr>
              <tr>
                <td>DOB</td>
                <td><input name="DOB" onChange={(event) => this.handleInut(event)} value={this.state.inputs.DOB} /></td>
              </tr>
              <tr>
                <td></td>
                <td><button onClick={this.addUser.bind(this)} >submit</button></td>
                {/* <td><button onClick={this.updateUser.bind(this)} >Update</button></td> */}
              </tr>
    
            </table>
    
            <DisplayCrud  UserData={this.state.UserData} />
           
            {/* <h2>{this.state.myInput}</h2> */}
          </div>
        );
      }
}

export default Crud;
