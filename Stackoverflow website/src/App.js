import React, { Component } from 'react';
import Header from './components/header/header';
import Footer from './components/footer/footer';
import './App.css';
import Body from './components/body/body';
import Toggle from './components/header/toggle/toggle';
import { BrowserRouter as Router, Route} from 'react-router-dom';


class App extends Component {
  render() {
    return (

        <Router>
      <div>
  
      <Route exact path="/footer" Component={Footer}/>
    
     
      
      
        <Header/>
        <Toggle/>
        <Body/>
        <Footer/>
        </div>
        </Router>
    
    );
  }
}

export default App;