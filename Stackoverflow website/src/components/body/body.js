import React, { Component } from 'react';
import './body.css';
import virat from '../images/Virat_Kohli.png';
import check from '../images/check-mark.svg';
import Top_button from '../images/305725.svg';
import Down_button from '../images/305725.png';
import StarIcon from '../images/Star.svg.png';
import Data from '../json/stackoverflow';


class Body extends Component {
    constructor(){
        super();
        this.state = {
            qunAns:Data
        };
    }
    
    componentWillMount() {
        debugger;
       console.log(Data);
      
       console.log(this.state.qunAns);

    
      }
    

    render() {
        const listsRows = this.state.qunAns[0].askQuestion.map((item,index) => {
          
            return   <div>
                            <i className="fa fa-check" aria-hidden="true"></i>
                             <span className="fs14">{item}</span>
                                    <br></br>
            </div>
                
            
          });
          console.log(listsRows);
        return (
            <div className="content-body">
                           <div className="container body-section">

                    <div className="d-flex justify-content-center">
                        <h2 className="content-title">Welcome to Stack Overflow</h2>
                    </div>
                    <img src="https://cdn.sstatic.net/Sites/stackoverflow/img/apple-touch-icon@2.png?v=73d79a89bded" className="site-icon"></img>
                    <div className="d-flex justify-content-center">
                        <div className="body-content container">
                            <p className="f20">
                                <b>Stack Overflow </b>is a question and answer site for professional and enthusiast programmers. It's built and run by you as part of the Stack Exchange network of Q&A sites. With your help, we're working together to build a library of detailed answers to every question about programming.
                                </p>
                                 <p className="helper-text d-flex justify-content-center">
                                We're a little bit different from other sites. Here's how:
                            </p>
                        </div>
                    </div>
                    <hr className="style7" />

                    <section className="ask-question">
                       
                            <div className="d-flex justify-content-center">
                                <h2 className="content-title">Ask questions, get answers, no distractions</h2>
                            </div>
                            <div className="row">
                            <div className="ask-question-left-side col-lg-5 col-5">
                                <p className="content-page-p">This site is all about <b>getting answers.</b> It's not a discussion forum. There's no chit-chat.</p>
                                <div className="just-question">
                                    Just question...
                                <div className="just-question-arrow"></div>
                                    <div className="just-ans">
                                        <p>...and answers.</p>
                                        <div className="just-ans-arrow"></div>
                                    </div>


                                </div>
                                <div className="good-answers">
                                <span className="accept-answers-img"><img className="accept-answer-img-svg" src={Top_button}/></span>
                                    <p>Good answers are voted up and <b>rise to the top</b>.</p>
                                    <p className="helper-text" style={{fontSize:"12px"}}>The best answers show up first so that they are always easy to find.</p>
                                </div>
                                <div className="accept-answers">
                                    <span className="accept-answers-img"><img className="accept-answer-img-svg" src={check}/></span>
                                    <p>The person who asked can mark one answer as "accepted".</p>
                                    <p className="helper-text" style={{fontSize:"12px"}}>Accepting doesn't mean it's the best answer, it just means that it worked for the person who asked.</p>
                                </div>
                            </div>
                            <div className="col-lg-7 col-7 shadow qa-block">
                                <div className="question-header">
                                    <h3 className="question-hyperlink">Do Swift-based applications work on OS X 10.9/iOS 7 and lower?</h3>
                                    <hr className="style7" style={{ marginTop: "5px" }} />
                                    <div className="row">
                                        <div className="col-lg-1">
                                        <img className="top_down_button_icon" src={Top_button} alt="" /><br></br>
                                        <p className="number-margins">3</p>
                                        <img className="top_down_button_icon" src={Down_button} alt="" /><br></br>
                                        <img className="top_down_button_icon" src={StarIcon} alt="" />
                                        </div>
                                        <div className="col-lg-11">
                                            <p className="fs14">Will Swift-based applications work on <a href="#" >OS X 10.9</a> (Mavericks)/iOS&nbsp;7 and lower?</p>
                                            <p className="fs14">For example, I have a machine running <a href="#" >OS X 10.8</a> (Mountain Lion), and I am wondering if an application I write in Swift will run on it.</p>
                                            <button type="button" className="btn btn-primary btn-xs" style={{ margin: "2px" }}>ios</button>
                                            <button type="button" className="btn btn-primary btn-xs" style={{ margin: "2px" }}>macos</button>
                                            <button type="button" className="btn btn-primary btn-xs" style={{ margin: "2px" }}>swift</button>

                                            <div className="owner">
                                                <div className="user-info">
                                                    <div className="user-action-time">
                                                        asked <span title="2014-06-02 19:25:57Z" className="relativetime" className="fs12">Jun 2 '14 at 19:25</span>
                                                    </div>
                                                    <div className="gravatar-wrapper-32"><img src="https://i.stack.imgur.com/xDQHx.jpg?s=32&amp;g=1" alt="" />

                                                        <span className="fs12">Ramu</span><span className="fs12">3,907<i className="fa fa-circle-thin" style={{ color: "green" }}></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h5 style={{ fontSize: "120%" }}>2 Answers</h5>
                                    <div className="row">
                                        <div className="col-lg-1 ">
                                        <img className="top_down_button_icon" src={Top_button} alt="" /><br></br>
                                        <p className="number-margins">3</p>
                                        <img className="top_down_button_icon" src={Down_button} alt="" />
                                        </div>
                                        <div className="col-lg-11">
                                            <p className="fs14">Swift code can be deployed to OS X 10.9 and iOS 7.0. It will usually crash at launch on older OS versions.</p>

                                            <div className="owner">
                                                <div className="user-info">
                                                    <div className="user-action-time">
                                                        asked <span title="2014-06-02 19:25:57Z" className="relativetime" className="fs12">Jun 2 '14 at 19:25</span>
                                                    </div>
                                                    <div className="gravatar-wrapper-32"><img src="https://i.stack.imgur.com/xDQHx.jpg?s=32&amp;g=1" alt="" />

                                                        <span className="fs12">Ramu</span><span className="fs12">3,907<i className="fa fa-circle-thin" style={{ color: "green" }}></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div  className="m50 row">
                                        <div className="col-lg-1">
                                        <img className="top_down_button_icon" src={Top_button} alt="" /><br></br>
                                        <p className="number-margins">3</p>
                                        <img className="top_down_button_icon" src={Down_button} alt="" />
                                        </div>
                                        <div className="col-lg-11">
                                            <p className="fs14">Apple has announced that Swift apps will be backward compatible with iOS 7 and OS X Mavericks. The WWDC app is written in Swift.</p>

                                            <div className="owner">
                                                <div className="user-info">
                                                    <div className="user-action-time">
                                                        asked <span title="2014-06-02 19:25:57Z" className="relativetime" className="fs12">Jun 2 '14 at 19:25</span>
                                                    </div>
                                                    <div className="gravatar-wrapper-32"><img src={virat} alt="" />

                                                        <span className="fs12">Ramu</span><span className="fs12">3,907<i className="fa fa-circle-thin" style={{ color: "green" }}></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>


                    </section>


                    <hr className="style7 m50"  />


                    <section className="practical-details">
                        <div className="d-flex justify-content-center">
                            <h2 className="content-title">Get answers to practical, detailed questions</h2>
                        </div>
                        <div className="row m50" >
                            <div className="col-lg-5 col-5">

                                <p className="fs14">
                                    <p>Focus on questions about an <b>actual problem</b> you have faced. Include details about what you have tried and exactly what you are trying to do.</p>
                                </p>
                            </div>

                            <div className="col-lg-7 col-5 shadow">
                                <p className="fs14">Ask about..</p>
                                <div style={{ marginLeft: "30px" }}>

                                    {listsRows}

                                    {/* <i className="fa fa-check" aria-hidden="true"></i>
                                    <span className="fs14">Specific programming problems</span>
                                    <br></br>
                                    <i className="fa fa-check" aria-hidden="true"></i>
                                    <span className="fs14">Software algorithms</span>
                                    <br></br>
                                    <i className="fa fa-check" aria-hidden="true"></i>
                                    <span className="fs14">Coding techniques</span>
                                    <br></br>
                                    <i className="fa fa-check" aria-hidden="true"></i>
                                    <span className="fs14">Software development tools</span>
                                    <br></br> */}

                                </div>

                            </div>
                        </div>
                        <div className="row m50" >
                            <div className="col-lg-5 col-5">

                                <p className="fs14">
                                    <p>Not all questions work well in our format. Avoid questions that are <b>primarily opinion-based</b>, or that are likely to <b>generate discussion</b> rather than answers.</p>
                                </p>
                                <p className="helper-text" style={{ fontSize: "12px", marginLeft: "20px" }}>Questions that need improvement may be <b>closed</b> until someone fixes them.</p>
                            </div>

                            <div className="col-lg-7 col-7 shadow">
                                <p className="fs14">Don't ask about...</p>
                                <div style={{ marginLeft: "30px" }}>
                                    <i className="fa fa-close" aria-hidden="true"></i>
                                    <span className="fs14">Questions you haven't tried to find an answer for (show your work!)</span>
                                    <br></br>
                                    <i className="fa fa-close" aria-hidden="true"></i>
                                    <span className="fs14">Requests for lists of things, polls, opinions, discussions, etc.</span>
                                    <br></br>
                                    <i className="fa fa-close" aria-hidden="true"></i>
                                    <span className="fs14">Anything not directly related to writing computer programs</span>
                                    <br></br>
                                    <i className="fa fa-close" aria-hidden="true"></i>
                                    <span className="fs14">Product or service recommendations or comparisons</span>
                                    <br></br>

                                </div>

                            </div>
                        </div>
                    </section>
                    
                    <hr className="style7 m50"  />

                    <section className="practical-details">
                        <div className="d-flex justify-content-center">
                            <h2 className="content-title">Tags make it easy to find interesting questions</h2>
                        </div>
                        <div className="row m50" >
                            <div className="col-lg-5 col-5">
                                <p className="fs14">All questions are <b>tagged</b> with their subject areas. Each can have up to 5 tags, since a question might be related to several subjects.</p>

                                <p style={{ marginTop: "50px", fontSize: "14px" }}><b>Click any tag</b> to see a list of questions with that tag, or go to the <a href="/tags">tag list</a> to browse for topics that interest you.</p>
                            </div>

                            <div className="col-lg-7 col-7 shadow qa-block">
                                <div className="question-header">
                                    <h3 className="question-hyperlink">Do Swift-based applications work on OS X 10.9/iOS 7 and lower?</h3>
                                    <hr className="style7" style={{ marginTop: "5px" }} />
                                    <div className="row">
                                        <div className="col-lg-1 col-1">
                                        <img className="top_down_button_icon" src={Top_button} alt="" /><br></br>
                                        <p style={{marginTop:"15px"}}>14</p>
                                        <img className="top_down_button_icon" src={Down_button} alt="" />
                                        </div>
                                        <div className="col-lg-11 col-7">
                                            <p className="fs14">Will Swift-based applications work on <a href="#" >OS X 10.9</a> (Mavericks)/iOS&nbsp;7 and lower?</p>
                                            <p className="fs14">For example, I have a machine running <a href="#" >OS X 10.8</a> (Mountain Lion), and I am wondering if an application I write in Swift will run on it.</p>
                                            <button type="button" className="btn btn-primary btn-xs" style={{ margin: "2px" }}>ios</button>
                                            <button type="button" className="btn btn-primary btn-xs" style={{ margin: "2px" }}>macos</button>
                                            <button type="button" className="btn btn-primary btn-xs" style={{ margin: "2px" }}>swift</button>

                                            <div className="owner">
                                                <div className="user-info">
                                                    <div className="user-action-time">
                                                        asked <span title="2014-06-02 19:25:57Z" className="relativetime" className="fs12">Jun 2 '14 at 19:25</span>
                                                    </div>
                                                    <div className="gravatar-wrapper-32"><img src="https://i.stack.imgur.com/xDQHx.jpg?s=32&amp;g=1" alt="" />

                                                        <span className="fs12">Ramu</span><span className="fs12">3,907<i className="fa fa-circle-thin" style={{ color: "green" }}></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </section>

                    <hr className="style7" />

                    <section className="practical-details">
                        <div className="d-flex justify-content-center">
                            <h2 className="content-title">You earn reputation when people vote on your posts</h2>
                        </div>
                        <div className="row comments-editing">
                            <div className="col-lg-5 col-5">
                                <p className="fs14">Your reputation score goes up when others <b>vote up</b> your questions, answers and edits.</p>
                            </div>
                            <div className="col-lg-3 col-3">
                                <div className="user-info-profile">
                                    <div className="user-info-logo">

                                    <img src="https://i.stack.imgur.com/xDQHx.jpg?s=32&amp;g=1" alt="" />
                                    </div>
                                    <span className="fs12">Ramu</span>
                                    <br></br>
                                    <span className="fs12">Telangana</span>
                                    <br></br><span>245</span>

                                </div>

                            </div>
                            <div className="col-lg-4 col-4 ">
                                <div className="rep-block fr">
                                    <span className="posts-voted">+5</span>
                                    answered is accepted
                        </div>
                                <div className="rep-block fr">
                                    <span className="posts-voted">+5</span>
                                    answered is accepted
                        </div>
                                <div className="rep-block fr">
                                    <span className="posts-voted">+5</span>
                                    answered voted up
                        </div>

                            </div>

                        </div>
                        <div className="row m50" >
                            <div className="col-lg-5 col-5">
                                <p className="fs14">
                                    As you earn reputation, you'll unlock new privileges like the ability to vote, comment, and even edit other people's posts.
                            </p>
                            </div>
                            <div className="col-lg-7 shadow col-7">
                                <table>
                                    <tr>
                                        <td className="fs15">Reputation</td>
                                        <td className="table-left">Privillag</td>
                                    </tr>
                                    <tr>
                                        <td className="fs15">15</td>
                                        <td className="table-left">Vote up</td>
                                    </tr>
                                    <tr>
                                        <td className="fs15">50</td>
                                        <td className="table-left">Leaves comments</td>
                                    </tr>
                                    <tr>
                                        <td className="fs15">125</td>
                                        <td className="table-left">Vote down (cost 1 rep on answers)</td>
                                    </tr>

                                </table>
                            </div>
                        </div>

                        <div className="row m50" >
                            <div className="col-lg-5 col-5">
                                <p className="fs14">
                                    <p>At the highest levels, you'll have access to special <b>moderation tools</b>. You'll be able to work alongside our <a href="/users?tab=moderators">community moderators</a> to keep the site focused and helpful.</p>
                                </p>
                            </div>
                            <div className="col-lg-7 col-7 shadow">
                                <table>
                                    <tr>
                                        <td className="fs15">Reputation</td>
                                        <td className="table-left">Privillag</td>
                                    </tr>
                                    <tr>
                                        <td className="fs15">15</td>
                                        <td className="table-left">Vote up</td>
                                    </tr>
                                    <tr>
                                        <td className="fs15">50</td>
                                        <td className="table-left">Leaves comments</td>
                                    </tr>
                                    <tr>
                                        <td className="fs15">125</td>
                                        <td className="table-left">Vote down (cost 1 rep on answers)</td>
                                    </tr>

                                </table>
                                <a href="#" style={{ textDecoration: "none" }}><p className="edit_comment">see all privileges</p></a>
                            </div>
                        </div>
                    </section>

                    <hr className="style7" />

                    <section className="practical-details">
                        <div className="d-flex justify-content-center">
                            <h2 className="content-title">Improve posts by editing or commenting</h2>
                        </div>
                        <div className="row comments-editing">
                            <div className="col-lg-5 col-5">
                                <div calssName="editing">
                                    <p>Our goal is to have the <b>best answers</b> to every question, so if you see questions or answers that can be improved, you can <b>edit</b> them.</p>
                                </div>
                                <p className="helper-text" style={{ fontSize: "14px", paddingLeft: "20px" }}>Use edits to fix mistakes, improve formatting, or clarify the meaning of a post.</p>

                                <div className="comments" >
                                    <div calssName="editing">
                                        <p>Use <b>comments</b> to ask for more information or clarify a question or answer.</p>
                                    </div>
                                    <p className="helper-text" style={{ fontSize: "14px", paddingLeft: "20px" }}>You can always comment on <b>your own</b> questions and answers. Once you earn 50 reputation, you can comment on anybody's post.</p>
                                    <p>Remember: we're all here to learn, so be friendly and helpful!</p>
                                </div>
                            </div>
                            <div className="col-lg-7 col-7 shadow comment-right">
                                <div className="row">
                                    <div className="col-lg-2 col-2">

                                    </div>
                                    <div className="col-lg-10 col-10">
                                        <p>Swift code can be deployed to OS X 10.9 and iOS 7.0. It will usually crash at launch on older OS versions.</p>
                                        <a href="#" style={{ textDecoration: "none" }}><p className="edit_comment">edit</p></a>
                                        <div className="post-signature">
                                            <div className="user-info">
                                                <span className="fs12">answered Jan 3 at 8:45 </span>
                                                <br></br> <img src="https://i.stack.imgur.com/xDQHx.jpg?s=32&amp;g=1" alt="" /><span style={{ fontSize: "13px", margin: "5px" }}>Ramu puppala</span>
                                                <br></br><span className="fs12"> 7,396 <i className="fa fa-circle-thin" style={{ color: "green" }}></i>1 <i className="fa fa-circle-thin" style={{ color: "green" }}></i>16<i className="fa fa-circle-thin" style={{ color: "green" }}></i>17</span>
                                            </div>
                                        </div>
                                        <br></br><br></br>
                                        <hr className="style7" />
                                        <div className="comment-footer">
                                            <p className="comment-p">Swift sets some bits in the Objective-C metadata to mark Swift classes. libobjc in OS X 10.9 and iOS 7.0 was changed to ignore these bits in preparation for Swift's arrival. Older OS versions will be confused by these bits. - Greg Parker Jun 21 '14 at 1:05</p>
                                            <a href="#" style={{ textDecoration: "none" }}><p className="edit_comment">add a comment</p></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </section>

                    <hr className="style7" />

                    <section className="practical-details">
                        <div className="d-flex justify-content-center">
                            <h2 className="content-title">Unlock badges for special achievements</h2>
                        </div>
                        <div className="row" style={{ marginTop: "35px" }}>
                            <div className="col-lg-5 col-5">
                                <p className="fs14">Badges are special achievements you earn for participating on the site. They come in three levels: bronze, silver, and gold.
                                </p>
                                <p className="fs14">In fact, <b>you've already earned a badge:</b></p>
                                <p><button type="button" className="btn btn-outline-primary" style={{ backgroundColor: "#0C0D0E", color: "white" }}>Informed</button> Read the entire tour page</p>
                            </div>
                            <div className="col-lg-7 col-7 shadow">
                                <div className="row">
                                    <div className="col-lg-5 col-5"><button type="button" className="btn btn-outline-primary" style={{ backgroundColor: "#0C0D0E", color: "white" }}>Student</button><br></br>
                                        <button type="button" className="btn btn-outline-primary m5" style={{ backgroundColor: "#0C0D0E", color: "white" }}>Editor</button>
                                        <button type="button" className="btn btn-outline-primary m5" style={{ backgroundColor: "#0C0D0E", color: "white" }}>Good Answer</button>
                                        <button type="button" className="btn btn-outline-primary m5" style={{ backgroundColor: "#0C0D0E", color: "white" }}>Civic Duty</button>
                                        <button type="button" className="btn btn-outline-primary m5" style={{ backgroundColor: "#0C0D0E", color: "white" }}>Famous Question</button>
                                    </div>
                                    <div classNae="col-lg-7 col-7">
                                        <p className="fs14 m5">	First question with score of 1 or more</p>
                                        <br></br>
                                        <p className="fs14 m5">	First edit</p><br></br>
                                        <p className="fs14 m5">		Answer score of 25 or more</p><br></br>
                                        <p className="fs14 m5">	Vote 300 or more times</p><br></br>
                                        <p className="fs14 m5">	Question with 10,000 views</p>

                                    </div>
                                    <a href=""  className="fs12 ml12">see all badges</a>
                                </div>
                            </div>
                        </div>
                    </section>

                    <hr className="style7" />

                    <section className="practical-details">
                        <div className="d-flex justify-content-center">
                            <h2 className="content-title">Find a question to answer, or ask your own</h2>
                        </div>
                        <div id="huge-buttons ">
                            <button type="button" className="btn btn-primary btn-lg btn-primary-left">See new questions</button>
                            <button type="button" className="btn btn-primary btn-lg btn-primary-right" >Ask new questions</button>
                            <p className="ask-qun-ans-p">Looking for more in-depth information on the site? <button type="button" className="btn btn-primary btn-lg" style={{ fontSize: "17px" }}>Ask new questions</button></p>
                        </div>
                    </section>
                    <hr className="style7" />
                    <section className="practical-details">
                        <div className="d-flex justify-content-center">
                            <h2 className="content-title">Stack Overflow is part of the Stack Exchange network</h2>
                        </div>

                        <div className="d-flex justify-content-center">
                            <div className="body-content container">

                                <p className="exchange-network-p d-flex justify-content-center">
                                    Like this site? Stack Exchange is a network of 173 Q&A sites just like it. Check out the full list of sites.
                                </p>
                                <img src="https://cdn.sstatic.net/Sites/stackexchange/img/apple-touch-icon@2.png?v=7512a9aa351b" className="site-icon" alt="Stack Exchange" />
                            </div>



                        </div>
                    </section>



                </div>


            </div>


        );
    }
}

export default Body;