import React, { Component } from 'react';
import './toggle.css';

class Hamburger extends React.Component {
    render() {
      return <div className="lines">
                  <span></span>
                  <span></span>
                  <span></span>
              </div>;
    }
}

class Menu extends React.Component {
    render() {
      return <div className="menu">
                 <ul>
                    <li>Home</li>
                    <li className="m30">Public
                        <ul style={{marginLeft:"-20px"}}>                       
                        <li><i className="fas fas-globe-europe"></i>Stack Overflow</li>
                            <li>Tags</li>                            
                            <li>Users</li>
                            <li>Jobs</li>
                        </ul>
                    </li>
                    <li>
                        <div className="shadow m30" style={{padding:"10px"}}>
                        <div className="row" style={{padding:"10px"}}>
                            <div className="col-lg-7"><p><b>Teams</b></p>
                            <p className="fs12">Q &amp; A for work</p>
                            </div>
                            <div className="col-lg-5" style={{float:"right"}}>
                            <svg width="53" height="49" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M49 11l.2 31H18.9L9 49v-7H4V8h31" fill="#CCEAFF"></path><path d="M44.5 19v-.3l-.2-.1-18-13-.1-.1H.5v33h4V46l.8-.6 9.9-6.9h29.3V19z" stroke="#1060E1" stroke-miterlimit="10"></path><path d="M31 2l6-1.5 7 2V38H14.9L5 45v-7H1V6h25l5-4z" fill="#fff"></path><path d="M7 16.5h13m-13 6h14m-14 6h18" stroke="#1060E1" stroke-miterlimit="10"></path><path d="M39 30a14 14 0 1 0 0-28 14 14 0 0 0 0 28z" fill="#FFB935"></path><path d="M50.5 14a13.5 13.5 0 1 1-27 0 13.5 13.5 0 0 1 27 0z" stroke="#F48024" stroke-miterlimit="10"></path><path d="M32.5 21.5v-8h9v8h-9zm2-9.5V9.3A2.5 2.5 0 0 1 37 6.8a2.5 2.5 0 0 1 2.5 2.5V12h-5zm2 3v2m1-2v2" stroke="#fff" stroke-miterlimit="10"></path></svg>
                            </div>
                        </div>
                        <button type="button" class="btn btn-outline-primary col-12">learn more</button>
                        </div>
                    </li>
                  
                 </ul>
             </div>;
    }
}

class Text extends React.Component {
render() {
  return <div className="text">
          
        </div>;
}
}

class Toggle extends React.Component {
constructor() {
   super();
   this.onButtonClick = this.onButtonClick.bind(this)
   this.state = { condition: true };
}

onButtonClick() {
   this.setState({ condition: !this.state.condition });
}
  
render() {
   return (
     <div>
        <div className={this.state.condition ? "hamburger inactive" :"hamburger active"}>
          <div onClick={this.onButtonClick}>
              <Hamburger />
          </div>
              <Menu />
          </div>  
           
     </div>
   );
 }
}

export default Toggle;