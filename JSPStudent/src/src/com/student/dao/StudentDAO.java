package src.com.student.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import src.com.student.bean.Student;
import src.com.student.util.DBUtil;

/**
 * 
 * @author This class used for Student connect to database and doing crud operations.
 *
 */
public class StudentDAO {

	/**
	 * create student data inserting into database with object 
	 * @param student
	 * @return
	 */
	
	public boolean createStudent(Student student) {

		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		
		String sql = "insert into Student_Details(name,email,pswd,address,state) values(?,?,?,?,?)";
		try {
			con = DBUtil.getCon();		
		
			
			ps = con.prepareStatement(sql);		
			ps.setString(1, student.getName());
			ps.setString(2,student.getEmail());
			ps.setString(3, student.getPswd());
			ps.setString(4, student.getAddrs());
			ps.setString(5,student.getState());
			
			int rowsEffected = ps.executeUpdate();
			
			if(rowsEffected == 1) {
				result = true;
			} 
		}
		catch (SQLException e){
			System.out.println(e);
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
	

	

	/**
	 * this method helps the STUDENT to set the username and password from the
	 * database
	 * 
	 * @param username sets the username
	 * @param password sets the password
	 * @return it returns the result to the service package
	 */

	public boolean doLogin(String username, String password) {

		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "select email,password from Student_Details where email = ? and password = ? ";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				result = true;				
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}
	
	/**
	 * It displays all the Student from the database
	 */

	
	public List<Student> displayStudent() {

		Connection con = null;
		List<Student> list = new ArrayList<Student>();
		String sql = "select * from Student_Details";
		try {
			con = DBUtil.getCon();
			Statement stmt = con.createStatement();
			ResultSet res = stmt.executeQuery(sql);
			while(res.next()) {
				Student a = new Student();
				a.setId(res.getInt(1));			
				a.setName(res.getString(2));
				a.setEmail(res.getString(3));
				a.setAddrs(res.getString(4));
				a.setPswd(res.getString(5));
				a.setState(res.getString(6));
				
				list.add(a);
			}			
		}
		catch (SQLException e){
			System.out.println("Values Cannot be Fetched");
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return list;
	}
	
	/**
	 * This method used for search student 
	 * @param search
	 * @return
	 */
	public Student doSearchStudent(int search)
	{
		Student student=null;
		Connection con = null;		
		PreparedStatement ps = null;		
		String sql = "select * from Student_Details where mail = ?  ";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, search);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
			    student = new Student();
				student.setId(rs.getInt(1));			
				student.setName(rs.getString(2));
				student.setPswd(rs.getString(3));
				student.setAddrs(rs.getString(4));
							
			}			
		}
		catch (SQLException e){
			System.out.println(e);
			student=null;
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return student;
	}
	/**
	 * This method used for the delete Student 
	 * @param deleteId
	 * @return
	 */
	public boolean doDeleteStudent(int deleteId) {
		Connection con = null;
		PreparedStatement ps = null;
		con = DBUtil.getCon();
		try {
			ps = con.prepareStatement("DELETE FROM Student_Details WHERE id= ?");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			ps.setInt(1, deleteId);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int result_set = 0;
		try {
			result_set = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (result_set > 0) {
			return true;
		} 
		return false;
	}
	/**
	 * This method used for update student details in database.
	 * @param student
	 * @return
	 */
	public static int doUpdateStudent(Student student) {
		Connection con = null;
		PreparedStatement ps = null;
		con = DBUtil.getCon();
		try {
			ps = con.prepareStatement("update Student_Details set name=?,pswd=?,email=?,address=?,state=? where id=?");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
		
			ps.setString(1,student.getName());
			ps.setString(2,student.getPswd());
			ps.setString(3,student.getEmail());
			ps.setString(4,student.getAddrs());
			ps.setString(5,student.getState());
			ps.setInt(6,student.getId());
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		int result_set = 0;
		try {
			result_set = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		
		}
		if (result_set > 0) {
			return result_set;
		} 
		return result_set;
	}
	
	/**
	 * Getting records from database to update the record 
	 * @param id
	 * @return
	 */
	public Student getRecordById(int id){  
		Student u=null;  
	    try{  
	        Connection con= DBUtil.getCon();
	        PreparedStatement ps=con.prepareStatement("select * from Student_Details where id=?");  
	        ps.setInt(1,id);  
	        ResultSet rs=ps.executeQuery();  
	        while(rs.next()){  
	            u=new Student();  
	            u.setId(rs.getInt("id"));  
	            u.setName(rs.getString("name"));  
	            u.setPswd(rs.getString("pswd"));  
	            u.setEmail(rs.getString("email"));  
	            u.setAddrs(rs.getString("address"));  
	            u.setState(rs.getString("state"));  
	        }  
	    }catch(Exception e){System.out.println(e);}  
	    return u;  
	} 
	
}
