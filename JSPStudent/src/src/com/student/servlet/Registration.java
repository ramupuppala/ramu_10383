package src.com.student.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import src.com.student.bean.Student;
import src.com.student.service.StudentService;

/**
 * Servlet implementation class Registration
 */
@WebServlet("/Registration")
public class Registration extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Registration() {
        super();
        // TODO Auto-generated constructor stub
    }



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("name");
		String email=request.getParameter("email");
		String pswd=request.getParameter("pswd");
		String repswd=request.getParameter("re-pswd");
		String addrs=request.getParameter("addrs");
		String state=request.getParameter("state"); 	
		
		Student student=new Student();
		
		response.setContentType("text/html");
		
		PrintWriter pw=response.getWriter();	

		HttpSession session=request.getSession(true);
		
		
	
		if(!pswd.equals(repswd))
		{
			pw.println("<script>alert('password doent match pleasse renter password');</script>");
			response.sendRedirect("AddStudent.jsp");
			
		} 
		else {
			
			
			student.setName(name);
			student.setPswd(pswd);
			student.setAddrs(addrs);
			student.setEmail(email);
			student.setAddrs(addrs);
			student.setState(state);
			
			StudentService ss=new StudentService();
			
			boolean result=ss.insertStudent(student);	
			
			
			if(!result)
			{
				session.setAttribute("email", email);
				pw.println("<script>alert('registration failed due to some problem');</script>");
				//response.sendRedirect("AddStudent.jsp");
			}
			else
			{
				request.getRequestDispatcher("index.html").include(request, response);
				pw.print("<h2>Succussfully registered");
			}
		}
		
		
		
	//	pw.println("</html></body>");
		pw.close();
	}

}
