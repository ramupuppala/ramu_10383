package com.student.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.student.bean.Student;
import com.student.service.StudentService;
import com.student.util.JsonConverter;

/**
 * Servlet implementation class Display
 */
@WebServlet("/Display")
public class Display extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Display() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
	    response.addHeader("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS, DELETE");
	    response.addHeader("Access-Control-Allow-Headers", "Content-Type");

	// TODO Auto-generated method stub
		response.setContentType("application/json");
		
		StudentService ss=new StudentService();
		List<Student> list = new ArrayList<Student>();
		list = ss.fetchStudent();
		

	
		PrintWriter pw=response.getWriter();		

		
		JsonConverter jsonConverter = new JsonConverter();
   		String result = jsonConverter.convertToJson(list);
   		pw.println(result);
   		pw.close();
	    
	}

	}

	



