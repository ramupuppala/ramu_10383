package src.com.student.servlet;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import src.com.student.bean.Student;
import src.com.student.service.StudentService;

/**
 * Servlet implementation class UpdateGetId
 */
@WebServlet("/UpdateGetId")
public class UpdateGetId extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateGetId() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int updateId=Integer.parseInt(request.getParameter("id"));
		
		StudentService ss=new StudentService();	
		
		Student student=ss.doUpdateGetId(updateId);
		
		request.setAttribute("student", student);
		request.getRequestDispatcher("UpdateForm.jsp").forward(request, response);	
		
		
		
		
	}

	

}
