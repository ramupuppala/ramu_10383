package src.com.student.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * This class is used for connection purpose
 * 
 * @author RANJHS
 *
 */

public class DBUtil {

	/**
	 * This method is used to load the driver and establish the connection
	 * 
	 * @return It returns the Connection
	 */

	public static Connection getCon() {
		Connection con = null;

		try {
			// 1.loading the driver
			Class.forName("com.mysql.jdbc.Driver");

			// 2.establish the connection
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Student_Details", "root", "ramu@123");
		

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// returns the connection
		return con;
	}

}
