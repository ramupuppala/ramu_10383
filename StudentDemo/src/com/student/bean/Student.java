package com.student.bean;

/**
 * This class used for bean for Student
 * @author IMVIZAG
 *
 */
public class Student {
private String name;
private int id;
private String password;
private String addrs;
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getAddrs() {
	return addrs;
}
public void setAddrs(String addrs) {
	this.addrs = addrs;
}

}
