package com.student.service;


import java.util.List;

import com.student.DAO.StudentDao;
import com.student.bean.Student;

public class StudentService {

	StudentDao studentDao = new StudentDao();

	/**
	 * this method helps to insert the student data into the database the result is
	 * retrieved from the student dao package
	 */

	public boolean insertStudent(Student student) {

		boolean result = studentDao.createStudent(student);
		return result;
	}

	/**
	 * this method helps to fetch the admin details from the database the result is
	 * retrieved from the dao package
	 */
	public List<Student> fetchStudent() {
		List<Student> display = studentDao.displayStudent();
		return display;
	}


	/**
	 * it helps the student to login into the application the result is retrieved from
	 * the dao package
	 */


	public boolean doLogin(String username, String password) {
		boolean result = studentDao.doLogin(username, password);
		return result;
	}


/**
 * This method used for Search for student.
 * @param search
 * @return
 */
	public Student doSearchStudent(int search)
	{
		Student student=new Student();
		student=studentDao.doSearchStudent(search);
		return student;
	}
	
	/**
	 * This method used for Delete student .
	 * @param deleteId
	 * @return
	 */
	
	public boolean doDelete(int deleteId)
	{
		boolean result=studentDao.doDeleteStudent(deleteId);
		return result;
	}
	
	/**
	 * This method used for Update student details
	 * @param student
	 * @return
	 */
	
	public boolean doUpdateStudent(Student student)
	{
		boolean result=studentDao.doUpdateStudent(student);
		return result;
	}
	
}
