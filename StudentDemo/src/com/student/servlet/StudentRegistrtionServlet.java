package com.student.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.student.bean.Student;
import com.student.service.StudentService;

/**
 * Servlet implementation class StudentRegistrtionServlet
 */
@WebServlet("/StudentRegistrtionServlet")
public class StudentRegistrtionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentRegistrtionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String name=request.getParameter("name");
		String password=request.getParameter("psw");
		String rePassword=request.getParameter("psw_repeat");
		String addr=request.getParameter("addr");
		int id=Integer.parseInt(request.getParameter("id"));
		
		
		
		
		Student student=new Student();
		
		response.setContentType("text/html");
		
		PrintWriter pw=response.getWriter();	
		
		
	//	pw.println("<html><body style=\"background-color:yellow;\r\n" + 
			//	"	margin-top: 14%;\r\n" + 
				//"    margin-left: 40%;\">");
		
		
		
	
		if(!rePassword.equals(password))
		{
			pw.println("<script>alert('password doent match pleasse renter password');</script>");
			ServletContext sc= getServletContext();
			
			sc.getRequestDispatcher("StudentRegistration.html").forward(request, response);
			
		} 
		else {
			
			student.setName(name);
			student.setPassword(password);
			student.setAddrs(addr);
			student.setId(id);
			
			StudentService ss=new StudentService();
			boolean result=ss.insertStudent(student);
			if(!result)
			{
				pw.println("<script>alert('registration failed due to some problem');</script>");
				pw.print("<h2>"+name +"</h2>");
			}
			else
			{
				request.getRequestDispatcher("Student.html").include(request, response);
				pw.print("<h2>Succussfully registered");
			}
		}
		
		
		
	//	pw.println("</html></body>");
		pw.close();
	}

}
