package com.student.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.student.bean.Student;
import com.student.service.StudentService;

/**
 * Servlet implementation class UpdateStudent
 */
@WebServlet("/UpdateStudent")
public class UpdateStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name=request.getParameter("name");
		String password=request.getParameter("psw");
		String rePassword=request.getParameter("psw-repeat");
		String addr=request.getParameter("addr");
		int id=Integer.parseInt(request.getParameter("id"));
		
		Student student=new Student();
		
		response.setContentType("text/html");
		
		PrintWriter pw=response.getWriter();
		
		HttpSession session = request.getSession(false);
		
		if( session != null)
		{
			request.getRequestDispatcher("Student.html").include(request, response);
			
			pw.println("<html><body style=\"background-color:yellow;\r\n" + 
					"	margin-top: 14%;\r\n" + 
					"    margin-left: 40%;\">");
			
			
		
			if(!rePassword.equals(password))
			{
				pw.println("<script>alert('password doent match pleasse renter password');</script>");
				ServletContext sc= getServletContext();
				
				sc.getRequestDispatcher("/StudentUpdate.html").forward(request, response);
				
			}
			else {
				
				student.setName(name);
				student.setPassword(password);
				student.setAddrs(addr);
				student.setId(id);
				
				StudentService ss=new StudentService();
				boolean result=ss.doUpdateStudent(student);
				if(!result)
				{
					pw.println("<script>alert('registration failed due to some problem');</script>");
					pw.print("<h2>"+name +"</h2>");
				}
				else
				{
					pw.print("<h2> Successfully Updated</h2>");
				}
			}
			
			
			
			pw.println("</html></body>");
		}
		else
		{
			request.getRequestDispatcher("Student.html").include(request, response);
			pw.print("<h2>You un authorized </h2>");
		}
		
		pw.close();
	}

	

	

}
