package com.student.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.student.bean.Student;
import com.student.service.StudentService;

/**
 * Servlet implementation class DisplayServlet
 */
@WebServlet("/DisplayServlet")
public class DisplayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		StudentService ss=new StudentService();
		List<Student> list = new ArrayList<Student>();
		
		
		PrintWriter pw=response.getWriter();
		
		HttpSession session=request.getSession();
		
		if(session.getAttribute("name") != null)
		{
			list = ss.fetchStudent();
			Iterator<Student> iterator = list.iterator();
			pw.println("<html><head>");
			String cssLocation = request.getContextPath()+"/css/style.css'";
		    String cssTag = "<link rel='stylesheet' type='text/css' href='" + cssLocation + "'>";	    
		    pw.println(cssTag+"</head><body style='background-color:red;'>");
		    
		    
		    pw.print("<table class='w3-table-all w3-card-4' style='    width: 32%;\r\n" + 
		    		"    margin-top: 16%;\r\n" + 
		    		"    margin-left: 33%;'><tr><th>Id</th><th>Name</th><th>Password</th><th>Address</th></tr>");

			
			while (iterator.hasNext()) {
				Student it = iterator.next();
				pw.println("<tr><td>"+it.getId()+"</td><td>"+it.getName()+"</td><td>"+it.getPassword()+"</td><td>"+it.getAddrs()+"</td></tr>");
//				System.out.printf("%-24s %-24s\n",it.getUserName(),it.getPassword());
				//System.out.println(it.getUserName() + " " + it.getPassword());
			}
			
			pw.println("</table></body></html>");
		}
		else
		{
			request.getRequestDispatcher("Student.html").include(request, response);
			pw.print("<h2>You un authorized </h2>");
		}
		
		  
		
	}

	}

	

