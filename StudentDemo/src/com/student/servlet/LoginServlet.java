package com.student.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.student.service.StudentService;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String name=request.getParameter("name");
		String password=request.getParameter("psw");	
		
	
		
		response.setContentType("text/html");
		
		PrintWriter pw=response.getWriter();
		HttpSession session=request.getSession(true);
		
		
		
		pw.println("<html><body style=\"background-color:yellow;\r\n" + 
				"	margin-top: 14%;\r\n" + 
				"    margin-left: 40%;\">");
	
		
			StudentService ss=new StudentService();
			boolean result=ss.doLogin(name,password);
			pw.print(result);
			if(!result)
			{
				
				pw.println("<h2>mismatch password please try again later");	
				pw.println("<script>alert('login failed' );</script>");
				ServletContext sc= getServletContext();
				sc.getRequestDispatcher("/StudentLogin.html").forward(request, response);
				
			}				
			else
			{
				session.setAttribute("name", name);
				request.getRequestDispatcher("Student.html").include(request, response);
				pw.print("<h2> Successfully login</h2>");
//				ServletContext sc= getServletContext();
//				
//				sc.getRequestDispatcher("/Student.html").forward(request, response);
//				
			}	
		
		
		pw.println("</html></body>");
		pw.close();
	}

}
