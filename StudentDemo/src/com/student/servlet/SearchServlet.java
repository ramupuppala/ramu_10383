package com.student.servlet;


import java.io.IOException;
import java.io.PrintWriter;


import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.student.bean.Student;
import com.student.service.StudentService;

/**
 * Servlet implementation class SearchStudent
 */
@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int search=Integer.parseInt(request.getParameter("id"));
		
		StudentService ss=new StudentService();	
		
		PrintWriter pw=response.getWriter();	
		
		Student student=new Student();
		
		HttpSession session=request.getSession(false);
		
		if(session != null)
		{
			student=ss.doSearchStudent(search);
			request.getRequestDispatcher("Student.html").include(request, response);
			pw.println("<html><head>");
			String cssLocation = request.getContextPath()+"/css/style.css'";
		    String cssTag = "<link rel='stylesheet' type='text/css' href='" + cssLocation + "'>";	    
		    pw.println(cssTag+"</head><body style='background-color:red;'>");
		    
		    
		    pw.print("<table class='w3-table-all w3-card-4' style='    width: 32%;\r\n" + 
		    		"    margin-top: 16%;\r\n" + 
		    		"    margin-left: 33%;'><tr><th>Id</th><th>Name</th><th>Password</th><th>Address</th></tr>");

			
		
				if(student != null)
				{
					pw.println("<tr><td>"+student.getId()+"</td><td>"+student.getName()+"</td><td>"+student.getPassword()+"</td><td>"+student.getAddrs()+"</td></tr>");
				}
				else
				{			
					ServletContext sc= getServletContext();
					
					sc.getRequestDispatcher("/StudentSearch.html").forward(request, response);
				}
				
		
			
			pw.println("</table></body></html>");
		}
		else
		{
			request.getRequestDispatcher("Student.html").include(request, response);
			pw.print("<h2>You un authorized </h2>");
		}
		
		
	}

	

}
