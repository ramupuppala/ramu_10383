package com.student.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.student.service.StudentService;

/**
 * Servlet implementation class DeleteServlet
 */
@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int deleteId=Integer.parseInt(request.getParameter("id"));
		
		StudentService ss=new StudentService();	
		
		PrintWriter pw=response.getWriter();
		HttpSession session=request.getSession(false);
		if(session != null)
		{
			boolean result=ss.doDelete(deleteId);
			pw.print("<html><head></head><body style='background-color:green;'>");
			if(result)
			{
				pw.print("<h2 style='margin:20%;'>Successfully deleted</h2>");
			}
			else
			{
				ServletContext sc= getServletContext();			
				sc.getRequestDispatcher("/StudentDelete.html").forward(request, response);
			}
		}
		else
		{
			
			request.getRequestDispatcher("Student.html").include(request, response);
			pw.print("<h2>Unauthorized user please</h2>");
		}
		
	}

	

}
