package com.MobileBillNew.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.MobileBillNew.util.DBUtil;

/**
 * This class used for Transaction for activating plans
 * @author ramu
 *
 */

public class AccountDAO {


	static Connection con = null;
	static PreparedStatement ps = null;
	//this method used for getamount from account 
	public static int getAmount(int accountNumber,String accountType) {

		
		int getData=0;
		String sql = "select account_id from "+accountType+" where id=?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, accountNumber);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int onlineAccountNumber=rs.getInt(1);
				System.out.println(onlineAccountNumber);
				getData=getAccount(onlineAccountNumber);
				
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return getData;
	}
	//this method used for Get account 
	public static int getAccount(int account)
	{
		int onlineAccountNumber=0;
		String sql = "select balance from Account where account_id=?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, account);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				onlineAccountNumber=rs.getInt(1);
						
				System.out.println(onlineAccountNumber);
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return onlineAccountNumber;
	}
	//this method used for get Account number 
	public static int getAccountNumber(int accountNumber,String accountType) {

		int onlineAccountNumber=0;
		
		String sql = "select account_id from  "+accountType+" where id=?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, accountNumber);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				onlineAccountNumber=rs.getInt(1);				
				
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return onlineAccountNumber;
	}
	//this method used for deposite money from account
	public static boolean doSendMoney(int amount,int accountNumber,String accountType)
	{
			
		int accountNumberAccount=getAccountNumber(accountNumber,accountType);
		
		boolean result=false;
		String sql = "update Account set balance = ? where account_id= ? ";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, amount);
			ps.setInt(2,accountNumberAccount);
			int rs = ps.executeUpdate();
			if (rs > 0) {
				result = true;
			}
			else {
				System.out.println("failed");
				return false;
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	public static void doAddingMoneyToAddmin(int amount,int accountNumber)
	{
			
		
		String sql = "update Account set balance = ? where account_id= ? ";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, amount);
			ps.setInt(2,accountNumber);
			int rs = ps.executeUpdate();
			if (rs > 0) {
				//result = true;
			}
			else {
				System.out.println("failed");
				
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

}
