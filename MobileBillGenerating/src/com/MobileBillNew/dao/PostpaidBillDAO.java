package com.MobileBillNew.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.MobileBillNew.util.DBUtil;

/**
 * This class is used for retrieving the expiry date for the PostPaid Users
 * 
 * @author RANJHS
 *
 */

public class PostpaidBillDAO {

	/**
	 * This method is used to get the expiry date
	 * 
	 * @param activation_id
	 * @return it returns the expirydate
	 */

	public static Date getExpiryDate(int activation_id) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Date expiryDate = null;
		// Date date = new Date(new java.util.Date().getTime());
		try {
			con = DBUtil.getCon();
			String sql = "select activation_id,expiry_date from activated_users where activation_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setInt(1, activation_id);
			rs = ps.executeQuery();
			while (rs.next()) {
				if (activation_id == rs.getInt(1)) {
					expiryDate = rs.getDate(2);
				}
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		// returns the result to the controller package
		return expiryDate;

	}

	/**
	 * this method is used to get the data used by the user
	 * @param activation_id
	 * @return
	 */
	
	public static String getDataUsed(int activation_id) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String data = null;
		// Date date = new Date(new java.util.Date().getTime());
		try {
			con = DBUtil.getCon();
			String sql = "select activation_id,dataUsage from activated_users1 where activation_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setInt(1, activation_id);
			rs = ps.executeQuery();
			while (rs.next()) {
				if (activation_id == rs.getInt(1)) {
					data = rs.getString(2);
				}
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		// returns the result to the controller package
		return data;

	}

	/**
	 * this method is used to get the plan id which user activated
	 * @param activation_id
	 * @return
	 */
	
	public static int getPlanId(int activation_id) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int plan_id = 0;
		// Date date = new Date(new java.util.Date().getTime());
		try {
			con = DBUtil.getCon();
			String sql = "select plan_id,activation_id from activated_users1 where activation_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setInt(1, activation_id);
			rs = ps.executeQuery();
			// System.out.println("hello");
			while (rs.next()) {
				if (activation_id == rs.getInt(2)) {
					plan_id = rs.getInt(1);
					// System.out.println(plan_id);
				}
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		// returns the result to the controller package
		return plan_id;

	}

	/**
	 * this method is used to get the actual data set by the admin
	 * @param plan_id
	 * @return
	 */
	
	public static String getActualData(int plan_id) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String data = null;
		// Date date = new Date(new java.util.Date().getTime());
		try {
			con = DBUtil.getCon();
			String sql = "select plan_id,data from plans where plan_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setInt(1, plan_id);
			rs = ps.executeQuery();
			while (rs.next()) {
				if (plan_id == rs.getInt(1)) {
					data = rs.getString(2);
				}
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		// returns the result to the controller package
		return data;
	}

	/**
	 * this function is used to get the amount for the plan which user is activated and set by the admin
	 * @param plan_id
	 * @return
	 */
	
	public static float getAmount(int plan_id) {
		float result = 0.0f;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DBUtil.getCon();
			String sql = "select plan_id,amount from plans where plan_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setInt(1, plan_id);
			rs = ps.executeQuery();
			while (rs.next()) {
				if (plan_id == rs.getInt(1)) {
					result = rs.getFloat(2);
				}
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		// returns the result to the controller package
		return result;
	}

	/**
	 * this function is used to get the price when ever the user uses excess data
	 * @return
	 */
	
	public static float getPriceFromDataUsage() {

		float price = 0.0f;
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			con = DBUtil.getCon();
			String sql = "select data_usage_id,price from dataUsage where data_usage_id = 1 ";
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				if (rs.getInt(1) == 1) {
					price = rs.getFloat(2);
				}
			}
		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		// returns the result to the controller package
		return price;
	}
	
	/**
	 * inserts data into the postpaid bill table
	 * @param activation_id
	 * @param dataUsed
	 * @param bill
	 * @return
	 */
	
	public static boolean setDataIntoTable(int activation_id,String dataUsed,float bill) {
		
		Connection con = null;
		PreparedStatement ps = null;
		//ResultSet rs = null;
		boolean result = false;
		try {
			con = DBUtil.getCon();
			String sql = " insert into postpaidbill(activation_id,DataUsage,bill) values(?,?,?)";
			ps = con.prepareStatement(sql);
			ps.setInt(1,activation_id);
			ps.setString(2,dataUsed);
			ps.setFloat(3,bill);
			int res = ps.executeUpdate();
			//System.out.println(res);
			if(res != 0) {
				result = true;
			}
		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		// returns the result to the controller package
		return result;

	}

}
