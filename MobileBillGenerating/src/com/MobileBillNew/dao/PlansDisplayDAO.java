package com.MobileBillNew.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.MobileBillNew.bean.Adminplans;
import com.MobileBillNew.util.DBUtil;

/**
 * This class is used to display all the plans to the user
 * 
 * @author RANJHS
 *
 */

public class PlansDisplayDAO {

	/**
	 * It displays all the SpecialPlans to the users
	 * 
	 * @return
	 */

	public static List<Adminplans> displaySpecialPlans() {

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Adminplans admin = null;
		List<Adminplans> list = new ArrayList<Adminplans>();
		try {
			con = DBUtil.getCon();
			String sql = "select * from plans where plan_category_id = 1";
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				admin = new Adminplans();
				admin.setPlan_name(rs.getString(2));
				admin.setData(rs.getString(3));
				admin.setCalls(rs.getInt(4));
				admin.setAmazon_prime(rs.getString(5));
				admin.setNetfix(rs.getString(6));
				admin.setAmount(rs.getInt(7));
				admin.setValidity(rs.getInt(8));
				list.add(admin);
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		// returns the result to the controller package
		return list;
	}

	/**
	 * It displays all the Dataplans to the users
	 * 
	 * @return It returns the list
	 */

	public static List<Adminplans> displayDataPlans() {

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Adminplans admin = null;
		List<Adminplans> list = new ArrayList<Adminplans>();
		try {
			con = DBUtil.getCon();
			String sql = "select * from plans where plan_category_id = 2";
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				admin = new Adminplans();
				admin.setPlan_name(rs.getString(2));
				admin.setData(rs.getString(3));
				admin.setCalls(rs.getInt(4));
				admin.setAmazon_prime(rs.getString(5));
				admin.setNetfix(rs.getString(6));
				admin.setAmount(rs.getInt(7));
				admin.setValidity(rs.getInt(8));
				list.add(admin);
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		// returns the result to the controller package
		return list;

	}

	/**
	 * It displays all the TalktimePlans to the users
	 * 
	 * @return It returns the list
	 */

	public static List<Adminplans> displayTalktimePlans() {

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Adminplans admin = null;
		List<Adminplans> list = new ArrayList<Adminplans>();
		try {
			con = DBUtil.getCon();
			String sql = "select * from plans where plan_category_id = 3";
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				admin = new Adminplans();
				admin.setPlan_name(rs.getString(2));
				admin.setData(rs.getString(3));
				admin.setCalls(rs.getInt(4));
				admin.setAmazon_prime(rs.getString(5));
				admin.setNetfix(rs.getString(6));
				admin.setAmount(rs.getInt(7));
				admin.setValidity(rs.getInt(8));
				list.add(admin);
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		// returns the result to the controller package
		return list;
	}

	/**
	 * It displays all the PostPaidPlans to the users
	 * 
	 * @return It returns the list
	 */

	public static List<Adminplans> displayPostpaidPlans() {

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Adminplans admin = null;
		List<Adminplans> list = new ArrayList<Adminplans>();
		try {
			con = DBUtil.getCon();
			String sql = "select * from plans where plan_category_id = 4";
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				admin = new Adminplans();
				admin.setPlan_name(rs.getString(2));
				admin.setData(rs.getString(3));
				admin.setCalls(rs.getInt(4));
				admin.setAmazon_prime(rs.getString(5));
				admin.setNetfix(rs.getString(6));
				admin.setAmount(rs.getInt(7));
				admin.setValidity(rs.getInt(8));
				list.add(admin);
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		// returns the result to the controller package
		return list;
	}

	/**
	 * It displays all the UnlimitedPlans to the users
	 * 
	 * @return It returns the list
	 */

	public static List<Adminplans> displayUnlimitedPlans() {

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Adminplans admin = null;
		List<Adminplans> list = new ArrayList<Adminplans>();
		try {
			con = DBUtil.getCon();
			String sql = "select * from plans where plan_category_id = 5";
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				admin = new Adminplans();
				admin.setPlan_name(rs.getString(2));
				admin.setData(rs.getString(3));
				admin.setCalls(rs.getInt(4));
				admin.setAmazon_prime(rs.getString(5));
				admin.setNetfix(rs.getString(6));
				admin.setAmount(rs.getInt(7));
				admin.setValidity(rs.getInt(8));
				list.add(admin);
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		// returns the result to the controller package
		return list;
	}
}
