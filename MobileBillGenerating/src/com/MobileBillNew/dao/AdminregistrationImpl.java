package com.MobileBillNew.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.MobileBillNew.bean.Admin;
import com.MobileBillNew.util.DBUtil;

/**
 * this class is used for the Admin to register into the application
 * @author RANJHS
 *
 */

public class AdminregistrationImpl implements AdminRegistration {

	/**
	 * It is used for the admin to register the details into the database
	 */
	
	@Override
	public boolean createAdmin(Admin admin) {

		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		
		String sql = "insert into Admin(username,password,security_answer) values(?, ?, ?)";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, admin.getUserName());
			ps.setString(2, admin.getPassword());
			ps.setString(3, admin.getSecurityAnswer());
			int rowsEffected = ps.executeUpdate();
			System.out.println("Rows Effected" +rowsEffected);
			if(rowsEffected == 1) {
				result = true;
			} 
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	/**
	 * It displays all the Admins from the database
	 */
	
	@Override
	public List<Admin> displayAdmin() {

		Connection con = null;
		List<Admin> list = new ArrayList<Admin>();
		String sql = "select * from admin";
		try {
			con = DBUtil.getCon();
			Statement stmt = con.createStatement();
			ResultSet res = stmt.executeQuery(sql);
			while(res.next()) {
				Admin a = new Admin();
				a.setUserName(res.getString(2));
				a.setPassword(res.getString(3));
				list.add(a);
			}			
		}
		catch (SQLException e){
			System.out.println("Values Cannot be Fetched");
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return list;
	}
}
