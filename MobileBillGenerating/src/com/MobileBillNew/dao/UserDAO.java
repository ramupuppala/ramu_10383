package com.MobileBillNew.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.MobileBillNew.util.DBUtil;

/**
 * this class is used to perform database connection for the user
 * 
 * @author IMVIZAG
 *
 */

public class UserDAO {

	/**
	 * this function is used to check whether the given mobile number is present or
	 * not
	 * 
	 * @param MobileNumber mobile number of an user
	 * @return service_id
	 */
	public static int validateUser(String MobileNumber) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int id = 0;
		try {
			con = DBUtil.getCon();
			String sql = "select service_id from service where mobile_number = ? ";
			ps = con.prepareStatement(sql);
			ps.setString(1, MobileNumber);
			rs = ps.executeQuery();
			while (rs.next()) {
				id = rs.getInt(1);
			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		return id;
	}

	/**
	 * this function is used to check the network of the user
	 * 
	 * @param mobileNumber mobile number of an user
	 * @return network of the user
	 */
	public static String networkOfUser(String mobileNumber) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String network = null;
		try {
			con = DBUtil.getCon();
			String sql = "select network from service where mobile_number = ? ";
			ps = con.prepareStatement(sql);
			ps.setString(1, mobileNumber);
			rs = ps.executeQuery();
			while (rs.next()) {
				network = rs.getString(1);
			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		return network;

	}

	/**
	 * this function is used to check whether the user mobile number is in
	 * registered DB or not
	 * 
	 * @param mobileNumber mobile number of an user
	 * @return true if number is present,false if number is not present
	 */
	public static boolean registeredUser(String mobileNumber) {
		boolean result = false;

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DBUtil.getCon();
			String sql = "select * from users where mobile_number = ? ";
			ps = con.prepareStatement(sql);
			ps.setString(1, mobileNumber);
			rs = ps.executeQuery();
			while (rs.next()) {
				result = true;
			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		return result;
	}

	/**
	 * this function is used to register an user
	 * 
	 * @param mobileNumber mobile number of an user
	 * @param name         name of user
	 * @param gender       gender of an user
	 * @param dob          date of birth of an user
	 * @param service_id   service id of the user(retrieved by service table)
	 * @return true if registered else false
	 */
	public static boolean userRegistration(String mobileNumber, String name, String gender, Date dob, int service_id) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		try {
			con = DBUtil.getCon();
			String sql = "insert into users(mobile_number,name,dob,gender,service_id) values(?,?,?,?,?) ";
			ps = con.prepareStatement(sql);
			ps.setString(1, mobileNumber);
			ps.setString(2, name);
			ps.setDate(3, dob);
			ps.setString(4, gender);
			ps.setInt(5, service_id);
			int res = ps.executeUpdate();
			if (res == 1) {
				result = true;
			}
		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		return result;
	}

}
