package com.MobileBillNew.Test;

import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.MobileBillNew.bean.Admin;
import com.MobileBillNew.service.AdminService;
import com.MobileBillNew.service.AdminServiceImpl;

public class AdminTest {

	AdminService service = null;

	/**
	 * TestCase method for creating the AdminService object It executes before each
	 * and every @Test(annotation) method
	 */
	@Before
	public void setUp() {
		service = new AdminServiceImpl();
	}

	/**
	 * TestCase method for storing null in the AdminService object It executes after
	 * each and every @Test(annotation) method
	 */
	@After
	public void tearDown() {
		service = null;
	}

	/**
	 * TestCase method for checking the positive values to insert the Admin data
	 */
	@Ignore
	@Test
	public void insertAdminPositive() {
		Admin admin = new Admin();
		admin.setUserName("harshitha");
		admin.setPassword("har@123");
		admin.setSecurityAnswer("maheshbabu");
		Assert.assertTrue(service.insertAdmin(admin));
	}

	/**
	 * TestCase method for checking the negative values to insert the Admin data
	 */
	@Ignore
	@Test
	public void insertAdminNegative() {
		Admin admin = new Admin();
		admin.setUserName("xyz");
		admin.setPassword("123");
		admin.setSecurityAnswer("hello");
		Assert.assertFalse(service.insertAdmin(admin));
	}

	/**
	 * TestCase method for checking the positive values to update the Admin data
	 */
	@Test
	public void updateAdminPositive() {

		Assert.assertTrue(service.updatePassword("harshitha", "harshi@123"));

	}

	/**
	 * TestCase method for checking the negative values to update the Admin data
	 */
	@Test
	public void updateAdminNegative() {

		Assert.assertFalse(service.updatePassword("harshi", "harshi"));

	}

	/**
	 * TestCase method for checking the Positive values to display the Admin data
	 */
	@Test
	public void fetchAdminPositive() {
		List<Admin> list = service.FetchAdmin();
		Assert.assertNotNull(list);
	}

	/**
	 * TestCase method for checking the Positive login credentials of Admin with
	 * username
	 */
	@Test
	public void doLoginUserNamePositive() {
		Assert.assertTrue(service.doLogin("harshitha"));
	}

	/**
	 * TestCase method for checking the Negative login credentials of Admin with
	 * username
	 */
	@Test
	public void doLoginUserNameNegative() {
		Assert.assertFalse(service.doLogin("harshi"));
	}

	/**
	 * TestCase method for checking the Positive login credentials of Admin with
	 * username and password
	 */
	@Test
	public void doLoginUserNamePasswordPositive() {
		Assert.assertTrue(service.doLogin("harshitha", "harshi@123"));
	}

	/**
	 * TestCase method for checking the Negative login credentials of Admin with
	 * username and password
	 */
	@Test
	public void doLoginUserNamePasswordNegative() {
		Assert.assertFalse(service.doLogin("harshi", "har"));
	}
}
