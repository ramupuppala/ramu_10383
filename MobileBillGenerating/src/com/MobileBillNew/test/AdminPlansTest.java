package com.MobileBillNew.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.MobileBillNew.bean.Adminplans;
import com.MobileBillNew.service.AdminPlansService;
import com.MobileBillNew.service.BankAccount;

/**
 * This class used for Testing of admin plans crud operations and Bank account 
 * @author IMVIZAG
 *
 */
public class AdminPlansTest {
	
	ArrayList<Adminplans> list = null;

	AdminPlansService service = null;
	
	@Before
	public void setUp() {
		service = new AdminPlansService();
		list=new ArrayList<Adminplans>();
	
	}
	
	@After
	public void tearDown() {
		
		service = null;
		list = null;
	}
	/**
	 * This method used for testing of unlimited plans, sending parameter of unlimited id =1 .
	 * 
	 * True case.......
	 */
	@Ignore
	@Test
	public void doDisplayPlansTestTrueCase() {
		
		list=service.doDisplayPlans(1);
		assertNotNull(list.size());
	}
	
	
	/**
	 * This method used for testing of unlimited plans, sending parameter of unlimited id =1 .
	 * 
	 * Negative case.......
	 */
	@Ignore
	@Test
	public void doDisplayPlansTestNegativeCase() {
		
		list=service.doDisplayPlans(1);
		assertEquals(0,list.size());
	}
	
	
	/**
	 * This method used for testing of unlimited plans and adding unlimited plans,with send parameter like object type of plan id.
	 */
	@Ignore
	@Test
	public void doAddingPlansTestTrueCase() {
		Adminplans ap=new Adminplans();
		ap.setPlan_name("RC299");
		ap.setData("unlimited pack");
		ap.setAmazon_prime("28");
		ap.setAmount(299);
		ap.setCalls(100);
		ap.setNetfix("28");
		ap.setValidity(28);	
		assertTrue(service.doAddingPlans(ap,3));	
		
	}
	
	
	/**
	 * This method used for testing of unlimited plans and adding unlimited plans,with send parameter like object type of plan id.
	 */
	@Ignore
	@Test
	public void doAddingPlansTestNegativeCase() {
		Adminplans ap=new Adminplans();
		ap.setPlan_name("RC199");
		ap.setData("unlimited pack");
		ap.setAmazon_prime("28");
		ap.setAmount(199);
		ap.setCalls(100);
		ap.setNetfix("28");
		ap.setValidity(28);	
		assertFalse(service.doAddingPlans(ap,3));	
		
	}
	
	/**
	 * This method used for testing of unlimited plans and deleting unlimited plans,with send parameters like plan type and plan Name.
	 */
	@Ignore
	@Test
	public void doDeletePlansTestTrueCase()
	{
		String planName="RC299";
		//Unlimited prepaid plans
		int planType=3;
		boolean result=service.doDeletePlans(planType, planName);
		assertTrue(result);	
		
	}
	
	/**
	 * This method used for testing of unlimited plans and deleting unlimited plans,with send parameters like plan type and plan Name.
	 * 
	 * 
	 */
	@Ignore
	@Test
	public void doDeletePlansTestNegativeCase()
	{
		String planName="RC199";
	
		int planType=3;
		boolean result=service.doDeletePlans(planType, planName);
		assertFalse(result);	
		
	}
	
	/**
	 * 
	 * This method used for getting balance from account using goolge pay 
	 */
	
	@Ignore
	@Test
	public void doGetGooglePayBalanceTrueCase()
	{
		int balance=7000;
		String accountType="googlepay";
		int amount=BankAccount.doGetBalance(balance,accountType);
		assertNotNull(amount);
	}
	
	/**
	 * 
	 * This method used for getting balance from account using goolge pay 
	 * 
	 * Negative case 
	 */
	
	@Ignore
	@Test
	public void doGetGooglePayBalanceNegativeCase()
	{
		int balance=7000;
		String accountType="googlepay";
		int amount=BankAccount.doGetBalance(balance,accountType);
		assertNull(amount);
	}
	
	/**
	 * This method used cut amount from user account.
	 */
	
	@Ignore
	@Test
	public void doGooglePaySendBalanceTestTrueCase()
	{
		int amount=7800;
		int accountNumber=13456;
		String accountType="googlepay";
		
		boolean result=BankAccount.doSendBalance(amount,accountNumber,accountType);
		assertTrue(result);
		
	}
	
	/**
	 * This method used cut amount from user account.
	 * 
	 * Negative case
	 */
	
	@Ignore
	@Test
	public void doGooglePaySendBalanceTestFalseCase()
	{
		int amount=7800;
		int accountNumber=13456;
		String accountType="googlepay";
		
		boolean result=BankAccount.doSendBalance(amount,accountNumber,accountType);
		assertFalse(result);
		
	}
	
	/**
	 * This method used for getting balance from account using payment pay 
	 */
	@Ignore
	@Test
	public void doGePaytmPayBalanceTrueCase()
	{
		int balance=8000;
		String accountType="paytm";
		int amount=BankAccount.doGetBalance(balance,accountType);
		assertNotNull(amount);
	}
	
	
	/**
	 * This method used for getting balance from account using payment pay 
	 * 
	 * Negative case
	 */
	@Ignore
	@Test
	public void doGePaytmPayBalanceNegativeCase()
	{
		int balance=8000;
		String accountType="paytm";
		int amount=BankAccount.doGetBalance(balance,accountType);
		assertNull(amount);
	}
	
	
	/**
	 * This method used cut amount from user account.
	 */
	@Ignore
	@Test
	public void doSendBalancePaytmTestTrueCase()
	{
		int amount=7800;
		int accountNumber=9949730;
		String accountType="paytm";
		
		boolean result=BankAccount.doSendBalance(amount,accountNumber,accountType);
		assertTrue(result);
		
	}
	

	/**
	 * This method used cut amount from user account.
	 * 
	 * Negative case
	 */
	@Ignore
	@Test
	public void doSendBalancePaytmTestNegativeCase()
	{
		int amount=7800;
		int accountNumber=9949730;
		String accountType="paytm";
		
		boolean result=BankAccount.doSendBalance(amount,accountNumber,accountType);
		assertFalse(result);
		
	}
	/**
	 *  This method used for adding amount to admin account 
	 */
	@Ignore
	@Test
	public void addingMoneytoadminAccountTestTrueCase()
	{
		int amount=8000;
		int accountNumber=62253522;
		boolean result=BankAccount.addingMoneytoadminAccount(amount,accountNumber);
		assertTrue(result);
		
	}
	/**
	 *  This method used for adding amount to admin account 
	 *  Negative case
	 */
	@Test
	public void addingMoneytoadminAccountTestNegativeCase()
	{
		int amount=8000;
		int accountNumber=62253522;
		boolean result=BankAccount.addingMoneytoadminAccount(amount,accountNumber);
		assertFalse(result);
		
	}
	
	
	
	
	
	

}
