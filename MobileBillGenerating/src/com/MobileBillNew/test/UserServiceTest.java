package com.MobileBillNew.Test;

import java.sql.Date;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.MobileBillNew.service.UserService;

public class UserServiceTest {

	UserService service = null;

	/**
	 * TestCase method for creating the AdminService object It executes before each
	 * and every @Test(annotation) method
	 */
	@Before
	public void setUp() {
		service = new UserService();
	}

	/**
	 * TestCase method for storing null in the AdminService object It executes after
	 * each and every @Test(annotation) method
	 */
	@After
	public void tearDown() {
		service = null;
	}

	/**
	 * Positive TestCase method for checking whether the user is registered in the
	 * network or not
	 */
	@Test
	public void registeredUserPositive() {

		Assert.assertTrue(UserService.registeredUser("9177995290"));
	}

	/**
	 * Negative TestCase method for checking whether the user is registered in the
	 * network or not
	 */
	@Test
	public void registeredUserNegative() {
		Assert.assertFalse(UserService.registeredUser("9876543210"));

	}

	/**
	 * Positive TestCase method for checking whether the user is valid
	 */
	@Test
	public void validateUserPositive() {
		int obj = UserService.validateUser("9177995290");
		Assert.assertTrue(obj==2);
		
	}
	
	/**
	 * Positive TestCase method for checking whether the user is valid
	 */
	@Test
	public void validateUserNegative() {
		int obj = UserService.validateUser("9177995290");
		Assert.assertFalse(obj==3);
	}
	
	//
	//	/**
	//	 * Positive TestCase method for checking the network of user
	//	 */
	//	@Test
	//	public void networkOfUserPositive() {
	//		String obj = UserService.networkOfUser("9177995290");
	//		Assert.assertTrue(obj=="prepaid");
	//	}
	//	
	//	/**
	//	 * Negative TestCase method for checking the network of user
	//	 */
	//	@Test
	//	public void networkOfUserNegative() {
	//		String obj = UserService.networkOfUser("9177995290");
	//		Assert.assertFalse(obj=="postpaid");
	//	}
	//	
	//	/**
	//	 * TestCase method for blocking the user
	//	 */
	//	@Test
	//	public void blockUser() {
	//		Assert.assertTrue(UserService.blockUser("9121719350"));
	//	}
	
	/**
	 * Positive TestCase method for checking whether the user is registered in the
	 * network or not
	 */
	@Test
	public void userRegisterationPositive() {
		Date dob = Date.valueOf("1995-05-15");
		Assert.assertTrue(UserService.userRegistration("9949730470", "Ram", "male", dob, 6));
	}

	/**
	 * Negative TestCase method for checking whether the user is registered in the
	 * network or not
	 */
	@Test
	public void userRegisterationNegative() {
		Date dob = Date.valueOf("1867-07-24");
		Assert.assertFalse(UserService.userRegistration("8466840323", "xyz", "male", dob, 78));
	}
	
}
