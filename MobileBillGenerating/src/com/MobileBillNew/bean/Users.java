package com.MobileBillNew.bean;

import java.util.Date;

/**
 * This AdminPlan class is used to declare,set the values for the variables and
 * retrive them
 * 
 * @author RANJHS
 *
 */
public class Users {
	// declaring the variables
	private String mobileNumber;
	private String name;
	private Date dob;
	private String gender;

	// getter and setter methods
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

}
