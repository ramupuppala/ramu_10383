package com.MobileBillNew.service;

import java.util.ArrayList;
import java.util.List;

import com.MobileBillNew.bean.Adminplans;
import com.MobileBillNew.dao.PlansDisplayDAO;

/**
 * This class is used to call the PlansDisplayDAO methods from the DAO package
 * 
 * @author RANJHS
 *
 */

public class PlansService {

	public static List<Adminplans> displaySpecialPlans() {

		List<Adminplans> list = new ArrayList<Adminplans>();
		list = PlansDisplayDAO.displaySpecialPlans();
		return list;

	}

	public static List<Adminplans> displayDataPlans() {

		List<Adminplans> list = new ArrayList<Adminplans>();
		list = PlansDisplayDAO.displayDataPlans();
		return list;

	}

	public static List<Adminplans> displayTalktimePlans() {

		List<Adminplans> list = new ArrayList<Adminplans>();
		list = PlansDisplayDAO.displayTalktimePlans();
		return list;

	}

	public static List<Adminplans> displayPostpaidPlans() {

		List<Adminplans> list = new ArrayList<Adminplans>();
		list = PlansDisplayDAO.displayPostpaidPlans();
		return list;

	}

	public static List<Adminplans> displayUnlimitedPlans() {

		List<Adminplans> list = new ArrayList<Adminplans>();
		list = PlansDisplayDAO.displayUnlimitedPlans();
		return list;

	}

}
