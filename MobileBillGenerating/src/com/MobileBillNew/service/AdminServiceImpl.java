package com.MobileBillNew.service;

import java.util.List;
import java.util.regex.Pattern;

import com.MobileBillNew.bean.Admin;
import com.MobileBillNew.dao.AdminLogin;
import com.MobileBillNew.dao.AdminregistrationImpl;

/**
 * this class implements all the methods from the interface
 * 
 * @author RANJHS
 *
 */

public class AdminServiceImpl implements AdminService {
	AdminregistrationImpl adminregistration = new AdminregistrationImpl();
	AdminLogin adminLogin = new AdminLogin();

	/**
	 * this method helps to insert the admin data into the database the result is
	 * retrieved from the dao package
	 */

	@Override
	public boolean insertAdmin(Admin admin) {

		boolean result = adminregistration.createAdmin(admin);
		return result;
	}

	/**
	 * this method helps to fetch the admin details from the database the result is
	 * retrieved from the dao package
	 */
	public List<Admin> FetchAdmin() {
		List<Admin> display = adminregistration.displayAdmin();
		return display;
	}

	/**
	 * it helps the admin to login into the application the result is retrieved from
	 * the dao package
	 */
	@Override
	public boolean doLogin(String username) {
		boolean result = adminLogin.doLogin(username);
		return result;

	}

	/**
	 * it helps the admin to login into the application the result is retrieved from
	 * the dao package
	 */

	@Override
	public boolean doLogin(String username, String password) {
		boolean result = adminLogin.doLogin(username, password);
		return result;
	}

	/**
	 * If the Admin forgets the password this helps the admin to update the password
	 * the result is retrieved from the dao package
	 */

	@Override
	public boolean updatePassword(String username, String password) {
		boolean result = adminLogin.updatePassword(username, password);
		return result;
	}
	
	public boolean validateData(String validateData)
	{
		boolean result=false;
		Pattern pattern = Pattern.compile("[A-Za-z0-9_]+");
		result = (validateData != null) && pattern.matcher(validateData).matches();
		if(validateData.length()<8)
		{
			result=false;
		}
		return result;

	}
	public boolean doSecurityanswer(String securityanswer,String username1)
	{
		boolean result=adminLogin.doSecurityanswerDAO( securityanswer, username1);
		
		return result;
		
	}
}