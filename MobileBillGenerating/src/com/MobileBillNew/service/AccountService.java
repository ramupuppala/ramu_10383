package com.MobileBillNew.service;

import com.MobileBillNew.dao.AccountDAO;

public class AccountService {

	public static int doGetBalance(int balance,String accountType)
	{
		int amount=AccountDAO.getAmount(balance,accountType);
		return amount;
	}
	public static boolean doSendBalance(int amount,int accountNumber,String accountType)
	{
		boolean result= AccountDAO.doSendMoney(amount,accountNumber,accountType);
		return result;
	}
	public static void addingMoneytoadminAccount( int amount,int accountNumber)
	{
		AccountDAO.doAddingMoneyToAddmin(amount,accountNumber);
	}
}
