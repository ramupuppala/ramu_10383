package com.mobilebill.servelt;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.MobileBillNew.bean.Admin;
import com.MobileBillNew.service.AdminServiceImpl;

/**
 * Servlet implementation class AdminServlet
 */
@WebServlet("/AdminServlet")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name=request.getParameter("name");
		String password=request.getParameter("psw");
		String rePassword=request.getParameter("psw-repeat");
		String securityAns=request.getParameter("securityAns");	
		
		Admin admin=new Admin();
		
		response.setContentType("text/html");
		
		PrintWriter pw=response.getWriter();
		
		
		
		
		pw.println("<html><body style=\"background-color:yellow;\r\n" + 
				"	margin-top: 14%;\r\n" + 
				"    margin-left: 40%;\">");
		
		
	
		if(!rePassword.equals(password))
		{
			pw.println("<script>alert('password doent match pleasse renter password');</script>");
			pw.print("<h2>"+name +"</h2>");   
			pw.print("<h2>"+password +"</h2>");  
			pw.print("<h2>"+rePassword +"</h2>"); 
			
		}
		else {
			admin.setUserName(name);
			admin.setPassword(password);
			admin.setSecurityAnswer(securityAns);
			
			AdminServiceImpl as=new AdminServiceImpl();
			boolean result=as.insertAdmin(admin);
			if(!result)
			{
				pw.println("<script>alert(registration failed due to some problem);</script>");
				pw.print("<h2>"+name +"</h2>");
			}
			else
			{
				pw.print("<h2> Successfully registred</h2>");
			}
		}
		
		
		
		pw.println("</html></body>");
		pw.close();
		
	}

	

}
