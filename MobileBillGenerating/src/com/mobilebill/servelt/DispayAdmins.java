package com.mobilebill.servelt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.MobileBillNew.bean.Admin;
import com.MobileBillNew.service.AdminServiceImpl;

/**
 * Servlet implementation class DispayAdmins
 */
@WebServlet("/DispayAdmins")
public class DispayAdmins extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DispayAdmins() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminServiceImpl as=new AdminServiceImpl();
		List<Admin> list = new ArrayList<Admin>();
		list = as.FetchAdmin();
		
		PrintWriter pw=response.getWriter();
		
		Iterator<Admin> iterator = list.iterator();
		
		HttpSession session=request.getSession(false);
		
		if(session != null)
		{
			pw.println("<html><head>");
			String cssLocation = request.getContextPath()+"/css/style.css'";
		    String cssTag = "<link rel='stylesheet' type='text/css' href='" + cssLocation + "'>";	    
		    pw.println(cssTag+"</head><body >");
		    
		   
		    
		    pw.print("<table class='w3-table-all w3-card-4' style='    width: 32%;\r\n" + 
		    		"    margin-top: 16%;\r\n" + 
		    		"    margin-left: 33%;'><tr><th>User Name</th><th>Password</tr>");

			
			while (iterator.hasNext()) {
				Admin it = iterator.next();
				pw.println("<tr><td>"+it.getUserName()+"</td><td>"+it.getPassword()+"</td></tr>");
//				System.out.printf("%-24s %-24s\n",it.getUserName(),it.getPassword());
				//System.out.println(it.getUserName() + " " + it.getPassword());
			}
			
			pw.println("</table></body></html>");
			  
			
		}
		else
		{
			request.getRequestDispatcher("AdminNavigation.html").include(request,response);
			pw.print("Your unauthorized !!!!!!!!!!!!!!!!!!!!!!!");
			
		}
		
		
	}

	

}
