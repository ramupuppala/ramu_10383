package com.mobilebill.servelt;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.MobileBillNew.service.AdminServiceImpl;

/**
 * Servlet implementation class AdminLogin
 */
@WebServlet("/AdminLogin")
public class AdminLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminLogin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String name=request.getParameter("name");
		String password=request.getParameter("psw");	
		
		response.setContentType("text/html");
		
		HttpSession session =request.getSession();
		
		PrintWriter pw=response.getWriter();	
		
		pw.println("<html><body>");
		
			AdminServiceImpl as=new AdminServiceImpl();
			boolean result=as.doLogin(name,password);
		
			if(!result)
			{ 			
				session.setAttribute("name", name);			
				request.getRequestDispatcher("AdminNavigation.html").include(request,response);
				pw.println("<h2>mismatch password please try again later");	
				
			}
				
			else
			{
				session.setAttribute("name",name);  	
				
				request.getRequestDispatcher("AdminNavigation.html").include(request, response);
				pw.print("<h2> Successfully login</h2>");
				
			}	
		
		
		pw.println("</html></body>");
		pw.close();
	
	}

}
